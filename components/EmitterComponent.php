<?php

/**
 * Yii component for Événement.
 *
 * @author Orlov Alexey <aaorlov88@gmail.com>
 * Library use https://github.com/igorw/evenement
 */

namespace yiicod\evenement\components;

use CApplicationComponent;
use CEvent;
use Evenement;
use Exception;
use Yii;

/**
 * Example listener config:.
 *
 * return array(
 *    'user.create' => array(
 *       array(Yii::app()->{component name}, '{method name}'),
 *       array(Yii::app()->{component name}, '{method name}'),
 *   ),
 *   'user.insert' => array(
 *       array(Yii::app()->{component name}, '{method name}'),
 *       array(Yii::app()->{component name}, '{method name}'),
 *   ),
 * );
 */
class EmitterComponent extends CApplicationComponent
{
    /**
     * Argument only class CEvent.
     */
    public $onlyCEvent = true;

    /**
     * Liastener php file.
     *
     * @var string
     */
    public $listeners = 'application.config.listeners';

    /**
     * Enabled.
     */
    protected $enabled = true;

    /**
     * @var object Evenement\EventEmitter
     */
    private $emitter = null;

    /**
     * Init component.
     */
    public function init()
    {
        parent::init();

        if (is_string($this->listeners)) {
            $listeners = include_once Yii::getPathOfAlias($this->listeners) . '.php';
        } elseif (is_array($this->listeners)) {
            $listeners = $this->listeners;
        } else {
            throw new Exception('Create ' . $this->listeners . '.php file or set array, it is requered! $listeners have to get array!');
        }

        foreach ($listeners as $key => $listener) {
            foreach ($listener as $objects) {
                if (true === is_array($objects) && false === is_object($objects[0]) && false === class_exists($objects[0])) {
                    $objects = function () use ($objects) {
                        $component = eval('return ' . $objects[0] . ';');
                        call_user_func_array([$component, $objects[1]], func_get_args());
                    };
                }

                $this->getEmitter()->on($key, $objects);
            }
        }
    }

    /**
     * @return Evenement\EventEmitter object
     */
    public function getEmitter()
    {
        if (null === $this->emitter) {
            $this->emitter = new Evenement\EventEmitter();
        }
        return $this->emitter;
    }

    /**
     * Call event.
     */
    public function emit($event, array $arguments = [])
    {
        $args = array_slice($arguments, 0, 1);
        $args = array_shift($args);
        if ($this->onlyCEvent && null !== $args && !$args instanceof CEvent) {
            throw new \CException('Argument only CEvent!');
        }
        if ($this->enabled) {
            $this->getEmitter()->emit($event, $arguments);
        }
    }

    /**
     * Add event.
     */
    public function on($event, callable $listener)
    {
        $this->getEmitter()->emit($event, $listener);
    }

    /**
     * Set enabled or disabled.
     *
     * @param bool $val
     */
    public function setEnable($val)
    {
        $this->enabled = $val;
    }
}
