Evenement extensions on Événement
=================================

If you want install to extensions folder, insert into composer.json:
--------------------------------------------------------------------
```php
"require": {
    "composer/installers": "1.0.3"
}
```

Config ( This is all config for extensions )
---------------------------------------------

Add component in main.php
-------------------------

```php
'components' => [
    'emitter' => [
        'class' => 'yiicod\evenement\components\EmitterComponent'
    ],
]

'preload' => ['emitter']

```

Write listener code
-------------------

class MailManager
{
    ...

    public function contactUs($event)
    {
            // some code here
    }
    ...
}

Register listener in listeners.php (application.config.listeners)
----------------------------------------------------------------

```php
return [

    ...

    'application.models.ContactUsModel.saveContact.success' => [
        [Yii::app()->mailManager, 'contactUs'] // register listener class and method
        ['Yii::app()->mailManager', 'contactUs'] // register listener class and method
    ],

    ...

];
```

Trigger event in saveContact method of ContactUsModel
---------------------------------------------
```php
Yii::app()->emitter->emit(
    'application.models.ContactUsModel.saveContact.success', [
        new CEvent($this, ['userDetailModel' => $this->userDetail]))
    ]
);
```